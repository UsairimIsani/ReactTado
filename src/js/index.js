import React , { Components , PropTypes }from "react";
import ReactDOM , { render } from "react-dom";
import { createStore } from "redux";
import "../css/main.scss";
import { Router, Route , hashHistory , IndxRoute} from "react-router";
import App from "./App.jsx";
const store = createStore(rootReducer);
render(
    <Provider store={store}>
        <Router>
            <Route path="/" component={App}></Route>
            <IndexRoute path="/signin" component={SignIn}></IndexRoute>
            <Route path="/todo" component={Todo}></Route>
          </Router>  
    </Provider>   
    
 , 
 
 document.getElementById("root"))
